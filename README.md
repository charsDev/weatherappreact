# WeatherAppReact

* I am learning React so I made my self a challenge to try to make the Weather App from  [Hamza Mira YT Channel](https://youtu.be/204C9yNeOYI)

* I use Webpack instead of create-react-app

* Can see it in [React App](http://honorable-mint.surge.sh/)

![alt text](https://image.ibb.co/jTWsTo/weather_react_app.jpg "Weather App screenshot")

* My bitbucket repo [WeatherApp Repo](https://bitbucket.org/charsDev/weatherappreact/src/master/)

Hope you like it 😀
import React from 'react'
import SearchContainer from '../../widgets/containers/searchContainer'
import './search.css'

const Search = props => (
  <div className="Search">
    <SearchContainer/>
  </div>
)

export default Search
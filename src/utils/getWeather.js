const API_KEY = "15ac9edf9b32ad076ae8c7b635d85e1b";

const getWeather = async(city, country) => {
  if (city && country ) {
    const api_call = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${API_KEY}&units=metric`)
    const data = await api_call.json()
    return data
  }else{
    const data = ''
    return data
  }
} 

export default getWeather
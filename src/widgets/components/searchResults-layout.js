import React from 'react'
import './searchResults-layout.css'

const SearchResults = props => (
  <div className="SearchResults">
    <p>Ciudad:
      <span>
        {props.data.city}
      </span>
    </p>
    <p>Pais:
      <span>
        {props.data.country}
      </span>
    </p>
    <p>Temperatura:
      <span>
        {props.data.temp}
      </span>
    </p>
    <p>Temperatura Maxima:
      <span>
        {props.data.temp_max}
      </span>
    </p>
    <p>Temperatura Minima:
      <span>
        {props.data.temp_min}
      </span>
    </p>
    <p>Actual:
      <span>
        {props.data.weather}
      </span>
    </p>
    <figure>
      <a href="https://bitbucket.org/charsDev/weatherappreact/src/master/">
        <img src="./images/Bitbucket.png" alt=""/>
      </a>
    </figure>
  </div>
)

export default SearchResults
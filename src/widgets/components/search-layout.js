import React from 'react'
import './search-layout.css'

const SearchLayout = props => (
  <div className="SearchLayout">
    {props.children}
  </div>
)

export default SearchLayout
import React from 'react'
import './searchForm.css'

const Search = props => (
  <div className='SearchForm'>
    <form
      autoComplete='off'
      onSubmit={props.handleSubmit}
    >
      <input
        name="city"
        type="text" 
        placeholder="City..."
      />
      <input
        name="country"
        type="text" 
        placeholder="Country..."
      />
      
      <button type="submit">Get Weather</button>
    </form>
  </div>
)

export default Search
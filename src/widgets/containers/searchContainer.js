import React, {Component} from 'react'
import Search from '../components/searchForm'
import SearchResults from '../components/searchResults-layout'
import getWeather from '../../utils/getWeather'
import SearchLayout from '../components/search-layout'
class SearchWeatherContainer extends Component {
  state= {
    data: {
      city: '',
      country: '',
      temp: '',
      temp_max: '',
      temp_min: '',
      weather: ''
    }
  }

  handleSubmit = async (event) => {
    event.preventDefault()

    const city = event.target.elements.city.value;
    const country = event.target.elements.country.value;
    
    const dataWeather = await getWeather(city, country)

    this.setState({
      data: {
        city: dataWeather.name,
        country: dataWeather.sys.country,
        temp: dataWeather.main.temp,
        temp_max: dataWeather.main.temp_max,
        temp_min: dataWeather.main.temp_min,
        weather: dataWeather.weather[0].description
      }
    })
}

  render() {
    return (
      <SearchLayout>
        <Search
          handleSubmit={this.handleSubmit}
        />
        <SearchResults data={this.state.data}/>
      </SearchLayout>
    )
  }
}

export default SearchWeatherContainer
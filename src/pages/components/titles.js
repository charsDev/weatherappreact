import React from 'react'
import './titles.css'

const Titles = props => (
  <div className="Titles">
    <h1>Weather Finder</h1>
    <p>Find out the temperature,</p>
    <p>weather conditions and more...</p>
  </div>
)

export default Titles
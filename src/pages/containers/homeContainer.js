import React, { Component } from 'react'
import HomeLayout from '../components/home-layout'
import Titles from '../components/titles'
import Search from '../../searchresults/components/search'

class Home extends Component {
  render() {
    return (
      <HomeLayout>
        <Titles />
        <Search />
      </HomeLayout>
    )
  }   
}

export default Home
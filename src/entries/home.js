import React from 'react'
import { render } from 'react-dom'
import Home from '../pages/containers/homeContainer'

const home = document.getElementById('home-container')

render(<Home  />, home)